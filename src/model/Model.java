package model;

public class Model {
    public static final int WIDTH = 520;

    public static final int HEIGHT = 520;

    private CellsOnGrid cells;

    private boolean pause = false;

    public CellsOnGrid getCells() {
        return cells;
    }

    public Model() {

        this.cells = new CellsOnGrid();
    }

    public void togglePause() {
        pause = !pause;
    }

    public void update() {
        if (!pause) {
            cells.toTheFuture();
        }
    }

    public void generateCell(int x, int y) {
        cells.generateFlip(x, y);
    }
}
