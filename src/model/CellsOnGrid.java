package model;

public class CellsOnGrid {

    private int numberRowsOfGrid = 50;
    private int numberColsOfGrid = 50;

    private Boolean[][] cellsOnGrid = new Boolean[numberRowsOfGrid][numberColsOfGrid];
    private int[][] neighbourGrid = new int[numberRowsOfGrid][numberColsOfGrid];

    public Boolean[][] getCellsOnGrid() {
        return cellsOnGrid;
    }

    public CellsOnGrid() {
        startConditions();
    }

    public int calculateNeighbours(int row, int col) {
        Boolean[][] curGen = cellsOnGrid;
        int rowsNum = numberRowsOfGrid;
        int colsNum = numberColsOfGrid;
        int numOfNeighbours = 0;
        // decides which neighbour cells to count (for edge cells
        // checks for neighbours from opposite edge)

        // not edge cells
        if ((row > 0) && (row < rowsNum - 1) && (col > 0) && (col < colsNum - 1)) {
            if (curGen[row - 1][col - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row - 1][col]) {
                numOfNeighbours++;
            }
            if (curGen[row - 1][col + 1]) {
                numOfNeighbours++;
            }
            if (curGen[row][col - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row][col + 1]) {
                numOfNeighbours++;
            }
            if (curGen[row + 1][col - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row + 1][col]) {
                numOfNeighbours++;
            }
            if (curGen[row + 1][col + 1]) {
                numOfNeighbours++;
            }
        }


        // top cells
        else if (row == 0) {

            // top-left cells
            if (col == 0) {

                // above
                if (curGen[rowsNum - 1][colsNum - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[rowsNum - 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[rowsNum - 1][col + 1]) {
                    numOfNeighbours++;
                }

                // same row
                if (curGen[row][colsNum - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row][col + 1]) {
                    numOfNeighbours++;
                }

                // below
                if (curGen[row + 1][colsNum - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row + 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[row + 1][col + 1]) {
                    numOfNeighbours++;
                }
            }

            // top-right cells
            else if (col == colsNum - 1) {

                // above
                if (curGen[rowsNum - 1][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[rowsNum - 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[rowsNum - 1][0]) {
                    numOfNeighbours++;
                }

                // same row
                if (curGen[row][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row][0]) {
                    numOfNeighbours++;
                }

                // below
                if (curGen[row + 1][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row + 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[row + 1][0]) {
                    numOfNeighbours++;
                }
            }

            // top but not left or right
            else {

                // above
                if (curGen[rowsNum - 1][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[rowsNum - 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[rowsNum - 1][col + 1]) {
                    numOfNeighbours++;
                }

                // same row
                if (curGen[row][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row][col + 1]) {
                    numOfNeighbours++;
                }

                // below
                if (curGen[row + 1][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row + 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[row + 1][col + 1]) {
                    numOfNeighbours++;
                }
            }
        }

        //bottom cells
        else if (row == rowsNum - 1) {

            // bottom-left cells
            if (col == 0) {

                // above
                if (curGen[row - 1][colsNum - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row - 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[row - 1][col + 1]) {
                    numOfNeighbours++;
                }

                // same row
                if (curGen[row][colsNum - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row][col + 1]) {
                    numOfNeighbours++;
                }

                // below
                if (curGen[0][colsNum - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[0][col]) {
                    numOfNeighbours++;
                }
                if (curGen[0][col + 1]) {
                    numOfNeighbours++;
                }
            }

            // bottom-right cells
            else if (col == colsNum - 1) {

                // above
                if (curGen[row - 1][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row - 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[row - 1][0]) {
                    numOfNeighbours++;
                }

                // same row
                if (curGen[row][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row][0]) {
                    numOfNeighbours++;
                }

                // below
                if (curGen[0][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[0][col]) {
                    numOfNeighbours++;
                }
                if (curGen[0][0]) {
                    numOfNeighbours++;
                }
            }

            // bottom but not left or right
            else {

                // above
                if (curGen[row - 1][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row - 1][col]) {
                    numOfNeighbours++;
                }
                if (curGen[row - 1][col + 1]) {
                    numOfNeighbours++;
                }

                // same row
                if (curGen[row][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[row][col + 1]) {
                    numOfNeighbours++;
                }

                // below
                if (curGen[0][col - 1]) {
                    numOfNeighbours++;
                }
                if (curGen[0][col]) {
                    numOfNeighbours++;
                }
                if (curGen[0][col + 1]) {
                    numOfNeighbours++;
                }
            }
        }

        // left but not top or bottom cells
        else if (col == 0) {

            // above
            if (curGen[row - 1][colsNum - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row - 1][col]) {
                numOfNeighbours++;
            }
            if (curGen[row - 1][col + 1]) {
                numOfNeighbours++;
            }

            // same row
            if (curGen[row][colsNum - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row][col + 1]) {
                numOfNeighbours++;
            }

            // below
            if (curGen[row + 1][colsNum - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row + 1][col]) {
                numOfNeighbours++;
            }
            if (curGen[row + 1][col + 1]) {
                numOfNeighbours++;
            }
        }

        // right but not top or bottom cells
        else if (col == colsNum - 1) {

            // above
            if (curGen[row - 1][col - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row - 1][col]) {
                numOfNeighbours++;
            }
            if (curGen[row - 1][0]) {
                numOfNeighbours++;
            }

            // same row
            if (curGen[row][col - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row][0]) {
                numOfNeighbours++;
            }

            // below
            if (curGen[row + 1][col - 1]) {
                numOfNeighbours++;
            }
            if (curGen[row + 1][col]) {
                numOfNeighbours++;
            }
            if (curGen[row + 1][0]) {
                numOfNeighbours++;
            }
        }

        return numOfNeighbours;
    }

    public void startConditions() {

        for (int i = 0; i < numberRowsOfGrid; i++) {
            for (int j = 0; j < numberColsOfGrid; j++) {
                if (Math.random() < 0.5) {
                    cellsOnGrid[i][j] = true;
                } else {
                    cellsOnGrid[i][j] = false;
                }
            }
        }
    }

    public void calculateNeighbourGrid() {

        for (int i = 0; i < numberRowsOfGrid; i++) {
            for (int j = 0; j < numberColsOfGrid; j++) {
                neighbourGrid[i][j] = calculateNeighbours(i, j);
            }
        }
    }

    public void willIBeAliveAgain() {
        for (int i = 0; i < numberRowsOfGrid; i++) {
            for (int j = 0; j < numberColsOfGrid; j++) {
                if (cellsOnGrid[i][j] == true) {
                    if (neighbourGrid[i][j] < 2 || neighbourGrid[i][j] > 3) {
                        cellsOnGrid[i][j] = false;
                    }
                    //der Fall, numberOfNeighbours ist 2 oder 3, betrifft den Rest
                    //in diesem Fall bleibt die Zelle am Leben
                } else {
                    if (neighbourGrid[i][j] == 3) {
                        cellsOnGrid[i][j] = true;
                    }
                }
            }
        }
    }

    public void toTheFuture() {
        calculateNeighbourGrid();
        willIBeAliveAgain();
    }

    public void generateFlip(int x, int y) {
        cellsOnGrid[x][y] = !cellsOnGrid[x][y];

    }

}


