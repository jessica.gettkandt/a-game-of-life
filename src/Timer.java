import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {

    private Model model;
    private Graphics graphics;


    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    @Override
    public void handle(long nowNano) {

        model.update();

        graphics.draw();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            System.out.println("got interrupted!");
        }
    }
}
