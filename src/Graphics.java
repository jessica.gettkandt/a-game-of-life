import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.Model;

import java.util.Random;

public class Graphics {

    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {

        Boolean[][] cellsOnGrid = this.model.getCells().getCellsOnGrid();


        Random random = new Random();
        float r = random.nextFloat();
        float g = random.nextFloat();
        float b = random.nextFloat();
        Color randomColor = Color.color(r, g, b);


        for (int i = 0; i < 50; i++) {

            for (int j = 0; j < 50; j++) {

                if (cellsOnGrid[i][j]) {
                    gc.setFill(Color.BLACK);
                    gc.fillRect(i * 10 + 10, j * 10 + 10, 10, 10);
                } else {
                    gc.setFill(Color.WHITE);
                    gc.fillRect(i * 10 + 10, j * 10 + 10, 10, 10);
                }

            }
            }
        }


}
