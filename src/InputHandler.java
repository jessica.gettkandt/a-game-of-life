import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.SPACE) {
            model.togglePause();
        }
    }

    public void onClick(double x, double y) {

        int coordinateX = (int) x / 10 - 1;
        int coordinateY = (int) y / 10 - 1;
        model.generateCell(coordinateX, coordinateY);
    }


}
